# Lorem
A simple Django project 

# Prerequisite
A Gitlab runner with helm and kubectl
* Add the lorem-ipsum helm repo
* Able to access k8s cluster
* Tag with `helm` and `k8s`

A Gitlab runner with docker
* Tag with `docker`
