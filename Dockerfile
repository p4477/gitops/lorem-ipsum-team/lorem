FROM python:3.7.12-alpine3.15

COPY . /webapps/lorem
WORKDIR /webapps/lorem

RUN pip install -r requirements.txt

ENTRYPOINT ["python", "/webapps/lorem/manage.py", "runserver", "0.0.0.0:8000"]